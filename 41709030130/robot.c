#include <stdio.h>  //fgets
#include <string.h> //strlen
#include "cJSON.h"
#include <curl/curl.h> //libcurl
#include <stdlib.h> //free

/*
 构造JSON请求报文
 {
    "perception": {
        "inputText": {
            "text": "你好"
        }
    },
    "userInfo": {
        "apiKey": "xxx",
        "userId": "suyujing"
    }
 }
 */
char* robot_make_request(const char* apikey, const char* text)
{
    //判断输入的字符串长度不为0
    if (strlen(text) == 0)
    {
        return NULL;
    }

    cJSON* request = cJSON_CreateObject();

    cJSON* perception = cJSON_CreateObject();
    cJSON* inputText = cJSON_CreateObject();

    cJSON_AddStringToObject(inputText, "text", text);
    cJSON_AddItemToObject(perception, "inputText", inputText);
    cJSON_AddItemToObject(request, "perception", perception);

    cJSON* userInfo = cJSON_CreateObject();
    cJSON_AddStringToObject(userInfo, "apiKey", apikey);
    cJSON_AddStringToObject(userInfo, "userId", "suyujing");
    cJSON_AddItemToObject(request, "userInfo", userInfo);

    //将JSON数据结构转为字符串
    return cJSON_Print(request);
}

char* robot_send_request(const char* request)
{
	FILE* fp;

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response, &resplen);
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("open_memstream() failed");
        return NULL;
    }

    //初始化HTTP客户端
    CURL* curl = curl_easy_init();
	
	//准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "http://openapi.tuling123.com/openapi/api/v2");
	
	//配置客户端，使用HTTP的POST方法发送请求消息
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    //配置需要通过POST请求消息发送给服务器的数据
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    //发送HTTP请求消息，等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        free(response);
        fclose(fp);
        return NULL;
    }
	
	//释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);

    //关闭内存文件
    fclose(fp);
	
	

	//解析JSON字符串
    cJSON* json = cJSON_Parse(response);
    if (json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if (error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        return NULL;
    }

	cJSON* results = cJSON_GetObjectItemCaseSensitive(json,"results");
	cJSON* array = cJSON_GetArrayItem(results,0);
	cJSON* values = cJSON_GetObjectItemCaseSensitive(array,"values");
	cJSON* text = cJSON_GetObjectItemCaseSensitive(values,"text");
	printf("机器人：%s\n", text->valuestring);
	
	//释放json数据结构占用的内存
    cJSON_free(json);

    free(response);

	return NULL;
}

//图灵机器人API，一次最多可以处理128个字符
#define LINE_LEN 128

//保存输入字符串的缓冲区
char line[LINE_LEN];

int main()
{
    char* apikey = "8fa15e1ef4fe4255ad85c5c9cedd8271";
    //从标准输入读取一行字符
    while(fgets(line, LINE_LEN, stdin) != NULL)
    {
        //构造请求报文
        char* request = robot_make_request(apikey, line);
        if (request == NULL)
        {
            continue;
        }
        //将请求报文发送给图灵机器人
        robot_send_request(request);
    }

    return 0;
}