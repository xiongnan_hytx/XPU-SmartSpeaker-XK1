#include <stdio.h>
#include <curl/curl.h> // libcurl的头文件
#include "cJSON.h"     //cJSON 头文件
#include <stdlib.h>

int main(void)
{
    FILE *fp;

    //响应消息的地址
    char *response = NULL;
    //响应消息的长度
    size_t resplen = 0;

    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response, &resplen);
    if (NULL == fp) // 打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return 1;
    }

    // 初始化libcurl
    CURL *curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return 1;
    }

    // 准备http请求信息,设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "http://www.nmc.cn/f/rest/aqi/52889");
    // 如果不指定写入的文件，libcurl会把服务器响应消息中的内容打印到屏幕中
    //如果指定了文件句柄，libcurl会把服务器响应消息中的内容写入文件
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    //发送http请求信息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return 1;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);

    //关闭内存文件后才能读取文件内容
    fclose(fp);

    //解析JSON字符串
    cJSON *json = cJSON_Parse(response);
    if (json == NULL)
    {
        const char *error_pos = cJSON_GetErrorPtr();
        if (error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        return EXIT_FAILURE;
    }
    cJSON *forecasttime = cJSON_GetObjectItemCaseSensitive(json, "forecasttime");
    cJSON *aqi = cJSON_GetObjectItemCaseSensitive(json, "aqi");
    cJSON *aq = cJSON_GetObjectItemCaseSensitive(json, "aq");
    cJSON *text = cJSON_GetObjectItemCaseSensitive(json, "text");

    printf("预报时间：%s\n", forecasttime->valuestring);
    printf("空气质量值：%d\n", aqi->valueint);
    printf("空气质量指数：%d\n", aq->valueint);
    printf("空气质量级别：%s\n", text->valuestring);

    //释放JSON数据结构占用的内存
    cJSON_free(json);

    free(response);

    return EXIT_SUCCESS;
}

// 查询结果：
// forecasttime：预测时间
// aqi：空气质量值
// aq：空气质量指数
// text：空气质量级别

