#include <stdio.h>
#include <stdlib.h> //malloc/free
#include <string.h> //strlen
#include <curl/curl.h> //libcurl
#include "cJSON.h"
#include "auth.h" //get_token
#include "robot.h"
#include "stt.h"
#include "tts.h"

int main()
{
	//指向音频数据的指针
	char* audio = NULL;
	//音频数据大小
	size_t size;
	//读取PCM文件内容
	size = stt_load_file("test.pcm", &audio);
	if (size == 0)
	{
		return EXIT_FAILURE;
	}

	char* token = get_token("c8zu0Ant0qfqV7bTM1Vac7PD", "rUDWYmcmNZKaH9abyiOaiOUbMBmTyIuu");
	if (NULL == token)
	{
		free(audio);
		return EXIT_FAILURE;
	}
	//将语音数据发送给云服务器，等待响应报文
	char* response1 = stt_send_request(token, audio, size);
	free(audio);
	if (response1 == NULL)
	{
		return EXIT_FAILURE;
	}
	
	//得到语音识别结果
	char* speak = stt_process_response(response1);
	if (NULL == speak)
	{
		return EXIT_FAILURE;
	}
	free(response1);

	char* apikey = "1b48bcfa708a430bbe4730ea9c4fb69a";
	
	//构造请求报文
	char* request = robot_make_request(apikey,speak);
		
	//将请求报文发送给图灵机器人
	char* response2 = robot_send_request(request);
	free(request);
	char* answer = robot_process_response(response2);
	free(response2);
	
	//将读入的文本转换为语音
	text2speech(token, answer);
	free(token);
	return 0;
}