
#include <stdio.h>
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h"
#include <stdlib.h>

int main(void)
{
	//以只写方式打开文件

	//FILE* fp = fopen("hello.txt", "w");
	char* response = NULL;
	size_t resplen = 0;
	FILE* fp = open_memstream(&response, &resplen);
	if (fp == NULL) //打开文件失败，打印错误信息并退出
	{
		perror("fopen_memstream() failed");
		return EXIT_FAILURE;
	}



	//初始化libcurl
	CURL* curl = curl_easy_init();
	if (curl == NULL)
	{
		perror("curl_easy_init() failed");
		return EXIT_FAILURE;
	}

	//准备HTTP请求消息，设置API地址（URI）
	curl_easy_setopt(curl, CURLOPT_URL, "http://www.nmc.cn/f/rest/aqi/57045");//
	//如果不指定写入的文件，libcurl会把服务器相应的消息打印到屏幕上
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	//发送HTTP请求消息，de服务器的响应消息
	CURLcode error = curl_easy_perform(curl);
	if (error != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
		curl_easy_cleanup(curl);
		return EXIT_FAILURE;
	}

	//释放libcurl申请的资源
	curl_easy_cleanup(curl);

	fclose(fp);

	puts(response);
	cJSON* json = cJSON_Parse(response);
	if (json == NULL){
		const char* error_pos = cJSON_GetErrorPtr();
		if (error_pos != NULL){
			fprintf(stderr, "Error before:%s\n", error_pos);
		}
		return EXIT_FAILURE;
	}
	cJSON* forecasttime = cJSON_GetObjectItemCaseSensitive(json, "forecasttime");
	cJSON* aqi = cJSON_GetObjectItemCaseSensitive(json, "aqi");
	cJSON* aq = cJSON_GetObjectItemCaseSensitive(json, "aq");
	cJSON* text = cJSON_GetObjectItemCaseSensitive(json, "text");
	printf("查询时间：%s\n", forecasttime->valuestring);
	printf("空气质量指数（aqi）：%d\n", aqi->valueint);
	printf("空气质量（aq）：%d\n", aq->valueint);
	printf("空气质量程度（text）：%s\n", text->valuestring);


	cJSON_free(json);

	return EXIT_SUCCESS;
}