#include <stdio.h>
#include <stdlib.h> //malloc/free
#include <string.h>
#include <curl/curl.h> //libcurl
#include "cJSON.h"
char* get_token(const char* api_key, const char* secret_key)
{
	FILE* fp;

	//响应消息的地址
	char* response = NULL;
	//响应消息的长度
	size_t resplen = 0;
	//创建内存文件，当通过文件句柄写入数据时，会自动分配内存
	fp = open_memstream(&response, &resplen);
	if (fp == NULL) //打开文件失败，打印错误信息并退出
	{
		perror("open_memstream() failed");
		return NULL;
	}

	//初始化HTTP客户端
	CURL* curl = curl_easy_init();

	char* uri = NULL;
	//拼接URI，asprintf会自动分配内存，并将要打印的字符串保存到内存中。
	asprintf(&uri,
		"https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s",
		api_key,
		secret_key);
	//准备HTTP请求消息，设置API地址（URI）
	curl_easy_setopt(curl, CURLOPT_URL, uri);
	//如果不指定写入的文件，libcurl会把服务器响应消息中的内容打印到屏幕上
	//如果指定了文件句柄，libcurl会把服务器响应消息中的内容写入文件
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	//发送HTTP请求消息，等待服务器的响应消息
	CURLcode error = curl_easy_perform(curl);
	if (error != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
		curl_easy_cleanup(curl);
		free(uri);
		fclose(fp);
		free(response);
		return NULL;
	}

	//释放HTTP客户端申请的资源
	curl_easy_cleanup(curl);
	//释放asprintf分配的内存
	free(uri);
	//关闭内存文件
	fclose(fp);
	cJSON* json = cJSON_Parse(response);
	if (json == NULL)
	{
		const char* error_pos = cJSON_GetErrorPtr();
		if (error_pos != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_pos);
		}
		free(response);
		return NULL;
	}

	//获取JSON报文中的access_token字段
	cJSON* access_token = cJSON_GetObjectItemCaseSensitive(json, "access_token");
	if (!cJSON_IsString(access_token)) //如果不存在access_token字段，进行错误处理
	{
		cJSON* error_description = cJSON_GetObjectItemCaseSensitive(json, "error_description");
		fprintf(stderr, "%s\n", error_description->valuestring);
		return NULL;
	}

	//复制token字符串，使用完之后需要使用free函数释放此内存
	char* token = strdup(access_token->valuestring);

	free(response);
	//释放cjson数据结构占用的内存
	cJSON_Delete(json);

	return token;
}
size_t stt_load_file(const char* file, char** pbuf)
{
	FILE* fp = fopen(file, "r");
	if (fp == NULL)
	{
		perror("fopen failed");
		return 0;
	}

	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);

	*pbuf = malloc(size);
	fseek(fp, 0, SEEK_SET);

	fread(*pbuf, 1, size, fp);
	fclose(fp);

	return size;
}
char* stt_process_response(const char* response)
{
	cJSON* json = cJSON_Parse(response);
	if (json == NULL)
	{
		const char* error_pos = cJSON_GetErrorPtr();
		if (error_pos != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_pos);
		}
		return NULL;
	}

	cJSON* err_no = cJSON_GetObjectItemCaseSensitive(json, "err_no");
	if (err_no->valueint != 0)
	{
		cJSON* err_msg = cJSON_GetObjectItemCaseSensitive(json, "err_msg");
		fprintf(stderr, "%s\n", err_msg->valuestring);
		return NULL;
	}

	cJSON* result = cJSON_GetObjectItemCaseSensitive(json, "result");
	//获取数组中的第一个元素
	cJSON* text = cJSON_GetArrayItem(result, 0);
	return text->valuestring;
}
char* robot_make_request(const char* apikey, const char* text)
{
	//判断输入的字符串长度不为0
	if (strlen(text) == 0)
	{
		return NULL;
	}

	cJSON* request = cJSON_CreateObject();

	cJSON* perception = cJSON_CreateObject();
	cJSON* inputText = cJSON_CreateObject();

	cJSON_AddStringToObject(inputText, "text", text);
	cJSON_AddItemToObject(perception, "inputText", inputText);
	cJSON_AddItemToObject(request, "perception", perception);

	cJSON* userInfo = cJSON_CreateObject();
	cJSON_AddStringToObject(userInfo, "apiKey", apikey);
	cJSON_AddStringToObject(userInfo, "userId", "liuyu");
	cJSON_AddItemToObject(request, "userInfo", userInfo);

	//将JSON数据结构转为字符串
	return cJSON_Print(request);
}
char* robot_send_request(const char* request)
{
	FILE* fp;
	//响应消息的地址
	char* response = NULL;
	//响应消息的长度
	size_t resplen = 0;
	//创建内存文件，当通过文件句柄写入数据时，会自动分配内存
	fp = open_memstream(&response, &resplen);
	if (fp == NULL) //打开文件失败，打印错误信息并退出
	{
		perror("open_memstream() failed");
		return NULL;
	}

	//初始化HTTP客户端
	CURL* curl = curl_easy_init();
	if (curl == NULL)
	{
		perror("curl_easy_init() failed");
		return NULL;
	}

	//准备HTTP请求消息，设置API地址（URI）
	curl_easy_setopt(curl, CURLOPT_URL, "http://openapi.tuling123.com/openapi/api/v2");
	//配置客户端，使用HTTP的POST方法发送请求消息
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	//配置需要通过POST请求消息发送给服务器的数据
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	//打印HTTP请求和响应消息头
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

	//发送HTTP请求消息，等待服务器的响应消息
	CURLcode error = curl_easy_perform(curl);
	if (error != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
		curl_easy_cleanup(curl);
		fclose(fp);
		free(response);
		return NULL;
	}

	//释放HTTP客户端申请的资源
	curl_easy_cleanup(curl);

	//关闭内存文件
	fclose(fp);

	return response;
}


char* robot_process_response(const char* response)
{
	cJSON* json = cJSON_Parse(response);
	if (json == NULL)
	{
		const char* error_pos = cJSON_GetErrorPtr();
		if (error_pos != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_pos);
		}
		return NULL;
	}

	cJSON* intent = cJSON_GetObjectItemCaseSensitive(json, "intent");
	if (intent == NULL)
	{
		fprintf(stderr, "Get intent object failed\n");
		cJSON_Delete(json);
		return NULL;
	}
	cJSON* code = cJSON_GetObjectItemCaseSensitive(intent, "code");
	if (code->valueint < 10000) //code字段的值如果小于10000，认为机器人出错
	{
		fprintf(stderr, "Robot error: %d\n", code->valueint);
		cJSON_Delete(json);
		return NULL;
	}

	cJSON* results = cJSON_GetObjectItemCaseSensitive(json, "results");
	cJSON* result;
	cJSON_ArrayForEach(result, results) //遍历数组
	{
		cJSON* resultType = cJSON_GetObjectItemCaseSensitive(result, "resultType");
		if (strcmp(resultType->valuestring, "text") == 0) //只打印文本消息
		{
			cJSON* values = cJSON_GetObjectItemCaseSensitive(result, "values");
			cJSON* text = cJSON_GetObjectItemCaseSensitive(values, "text");
			return text->valuestring;
		}
	}

	cJSON_Delete(json);
}
void text2speech(const char* token, const char* text)
{
	CURL* curl = curl_easy_init();

	//发送到百度云的字符串需要进行2次URL编码
	char* temp = curl_easy_escape(curl, text, strlen(text));
	char* data = curl_easy_escape(curl, temp, strlen(temp));
	curl_free(temp);

	//拼接POST请求发送的数据
	char* postdata;
	asprintf(&postdata, "tex=%s&lan=zh&cuid=hqyj&ctp=1&aue=6&tok=%s", data, token);
	curl_free(data);

	//启动播放软件，通过管道写入音频数据
	FILE* fp = popen("aplay -q -", "w");
	if (fp == NULL)
	{
		perror("fopen() failed");
		return;
	}

	curl_easy_setopt(curl, CURLOPT_URL, "https://tsn.baidu.com/text2audio");
	//配置客户端，使用HTTP的POST方法发送请求消息
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	//配置需要通过POST请求消息发送给服务器的数据
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	//发送HTTP请求消息，等待服务器的响应消息
	CURLcode error = curl_easy_perform(curl);
	if (error != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
		curl_easy_cleanup(curl);
		free(postdata);
		pclose(fp);
		return;
	}

	//释放HTTP客户端申请的资源
	curl_easy_cleanup(curl);
	free(postdata);

	//关闭管道
	pclose(fp);
}
char* stt_send_request(const char* token, const char* audio, size_t size)
{
	FILE* fp;
	//响应消息的地址
	char* response = NULL;
	//响应消息的长度
	size_t resplen = 0;
	//创建内存文件，当通过文件句柄写入数据时，会自动分配内存
	fp = open_memstream(&response, &resplen);
	if (fp == NULL) //打开文件失败，打印错误信息并退出
	{
		perror("open_memstream() failed");
		return NULL;
	}

	//初始化HTTP客户端
	CURL* curl = curl_easy_init();
	if (curl == NULL)
	{
		perror("curl_easy_init() failed");
		return NULL;
	}

	//拼接API地址和参数
	char* url = NULL;
	asprintf(&url, "http://vop.baidu.com/server_api?cuid=liuyu&token=%s&dev_pid=1537", token);

	//准备HTTP请求消息，设置API地址（URI）
	curl_easy_setopt(curl, CURLOPT_URL, url);
	//创建请求头链表
	struct curl_slist* headers = NULL;
	//向链表中增加头部字段
	headers = curl_slist_append(headers, "Content-Type: audio/pcm;rate=16000");
	//设置HTTP请求头部字段
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	//配置客户端，使用HTTP的POST方法发送请求消息
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	//配置需要通过POST请求消息发送给服务器的数据
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, audio);
	//指定发送数据的长度
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, size);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
	//打印HTTP请求和响应消息头
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

	//发送HTTP请求消息，等待服务器的响应消息
	CURLcode error = curl_easy_perform(curl);
	if (error != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
		curl_easy_cleanup(curl);
		fclose(fp);
		free(response);
		free(url);
		return NULL;
	}

	//释放HTTP客户端申请的资源
	curl_easy_cleanup(curl);

	//关闭内存文件
	fclose(fp);

	//释放asprintf申请的内存
	free(url);

	return response;
}
//百度语音合成API，一次最多可以转换2048个字符
#define LINE_LEN 2048

//保存输入字符串的缓冲区
char line[LINE_LEN];
int main()
{
	//指向音频数据的指针
	char* audio = NULL;
	//音频数据大小
	size_t size;
	//读取PCM文件内容
	size = stt_load_file("test.pcm", &audio);
	if (size == 0)
	{
		return EXIT_FAILURE;
	}

	char* token = get_token("E18KTEN5Nt3mgyYUNcZoGTa9", "TileijvncAVHVBOQZyEVme0XFD0AqrTZ");
	if (NULL == token)
	{
		free(audio);
		return EXIT_FAILURE;
	}
	//将语音数据发送给云服务器，等待响应报文
	char* response = stt_send_request(token, audio, size);
	free(audio);
	if (response == NULL)
	{
		return EXIT_FAILURE;
	}
	//puts(response);
	//处理响应报文，将文本信息打印到屏幕上
	char* text = stt_process_response(response);
	free(response);
	puts(text);
	char* apikey = "3f6d5a2d3ab74912ad5aeb667aa99125";
	char* request = robot_make_request(apikey, text);
	//将请求报文发送给图灵机器人
	char* response1 = robot_send_request(request);
	free(request);
	char* text_2 = robot_process_response(response1);
	free(response1);
	puts(text_2);
	char* token_2 = get_token("gG1oXkYhqC72KbsfKNawHZNv", "ijAXKgNwUaQZbbVXryZmef6DnouQ6EnS");
	text2speech(token_2,text_2);
	return 0;
}