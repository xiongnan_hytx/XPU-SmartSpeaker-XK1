#include <stdio.h>
#include <string.h>
#include <stdlib.h> //malloc/free
#include <curl/curl.h> //libcurl
#include "auth.h" //get_token
#include "cJSON.h"
#include "stt.h"
#include "robot.h"
#include "tts.h"

//#define LINE_LEN 128
//
////保存输入字符串的缓冲区
//char line[LINE_LEN];

size_t stt_load_file(const char* file, char** pbuf)
{
    FILE* fp = fopen(file, "r");
    if (fp == NULL)
    {
        perror("fopen failed");
        return 0;
    }

    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);

    *pbuf = malloc(size);
    fseek(fp, 0, SEEK_SET);

    fread(*pbuf, 1, size, fp);
    fclose(fp);

    return size;
}

int main()
{
    //指向音频数据的指针
    char* audio = NULL;
    //音频数据大小
    size_t size;
    //读取PCM文件内容
    size = stt_load_file("test.pcm", &audio);
    if (size == 0)
    {
        return EXIT_FAILURE;
    }

    char* token = get_token("QdCoOj3103fU5qABUu70M8Xt", "HOPAaoUQEilsdA0e81aWKF9IbnKC7eIY");
    if (NULL == token)
    {
        free(audio);
        return EXIT_FAILURE;
    }
    //将语音数据发送给云服务器，等待响应报文
    char* response_1 = stt_send_request(token, audio, size);
    free(audio);
    if (response_1 == NULL)
    {
        return EXIT_FAILURE;
    }
    //puts(response);
    //处理响应报文，将文本信息打印到屏幕上
    char* send_text = stt_process_response(response_1);
//    puts(send_text);
    char* apikey = "fb5ffcf62c644f1f87396dbeba2135b2";
    char* request = robot_make_request(apikey, send_text);
//    if (request == NULL)
//    {
//        continue;
//    }
    //将请求报文发送给图灵机器人
    char* response_2 = robot_send_request(request);
    free(request);
//    if (response_2 == NULL)
//    {
//        continue;
//    }
    char* apply_text = robot_process_response(response_2);
    puts(apply_text);
    free(response_2);
    free(response_1);
    text2speech(token, apply_text);
    free(token);
    
}
