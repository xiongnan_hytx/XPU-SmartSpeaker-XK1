#ifndef ROBOT_H
#define ROBOT_H

char* robot_make_request(const char* apikey, const char* text);
char* robot_send_request(const char* request);
char* robot_process_response(const char* response);

#endif /* 避免重复定义 */
